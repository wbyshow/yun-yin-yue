import config from './config'
// 发送ajax请求

export default (url, data = {}, method = 'GET') => {
	return new Promise((resolve, reject) =>
		//初始化promise状态为pending
		wx.request({
			url: config.host + url,
			data,
			method,
			success: (res) => {
				console.log('请求成功', res)
				resolve(res.data)
			},
			fail: (err) => {
				console.log('请求失败', err)
				reject(err)
			},
		})
	)
}
